#!/bin/bash
echo "hello world"
cd ~
###########################
## Testing S3 connectivity
#echo "writing the file"
#echo "hello" > world2.txt
#echo "listing local files:"
#echo $(ls -l)
#echo "listing s3 buckets:"
#echo $(aws s3 ls)
#echo "uploading"
#aws s3 cp world2.txt s3://scottylerlabarchives/
#echo "done uploading"
###########################
## Test install of PyMINEr
echo $(python3 -V)
echo "installing appropriate software"
#yum upgrade && \
sudo yum -y install xorg-x11-xauth  xclock xterm ## get X11 going to prevent any errors with plotting
sudo service sshd restart
    sudo yum -y  groupinstall "Development Tools" && \
    #sudo yum install -y devtoolset && \
    sudo yum -y install gcc gcc-c++ libstdc++ g++ clang && \
    sudo yum -y install gcc zlib-devel bzip2 bzip2-devel readline-devel sqlite sqlite-devel openssl-devel tk-devel libffi-devel xz-devel gdbm-devel ncurses-devel db4-devel wget && \
    wget https://www.python.org/ftp/python/3.7.9/Python-3.7.9.tgz
    tar -xzvf Python-3.7.*
    sudo sh Python-3.7.*/configure --enable-optimizations
    sudo make altinstall
    #sudo ln -s /usr/local/bin/python3.7 /usr/bin/python3
    #sudo ln -s /usr/local/bin/pip3.7 /usr/bin/pip3
#    yum install -y python3.7 python3-pip python3-devel && \
    echo $(python3 -V) && \
    sudo python3 -m pip install numpy chardet==3.0.4 && \
    sudo python3 -m pip install  bio-pyminer==0.9.8
echo $(pyminer.py -h)
###
##apt-get upgrade && \
##    apt-get update && \
##    #apt-get install -y python3.7 python3-pip && \
##    apt-get install -y python3-pip && \
##    echo $(python3 -V) && \
##    python3 -m pip install numpy chardet==3.0.4 && \
##    python3 -m pip install  bio-pyminer==0.9.8
pyminer.py -h > pyminer_help_text.txt
aws s3 cp pyminer_help_text.txt s3://scottylerlabarchives/
###########################

mkdir ~/data/
cd ~/data/
aws s3 cp s3://scottylerlabarchives/pyminer_analyses/example/airway_merged.txt .
tab_to_h5.py airway_merged.txt
pyminer.py -hdf5 -i airway_merged.hdf5 -ID_list ID_list.txt -columns column_IDs.txt -species mmusculus -block_size 1000 -beta_test -ap_clust > pyminer_output.log
#ls -l > all_files.txt
#aws s3 cp . s3://scottylerlabarchives/pyminer_analyses/example/output/ --recursive --exclude .hdf5
#sudo shutdown -h now
